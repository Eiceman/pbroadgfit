This document covers some points of the basic purpose, and use of these programs.  The original paper (IN-PROCEEDINGS), offers much more detail about their use.  Additionally, more documentation can be found in the doc folder of this respository, also please see our [LICENSE](LICENSE) and [CITATION](CITATION) files.

## About gFit and pBroad Programs

Originally these two suites of programs were developed for the paper *"Re-evaluation of ortho-para-dependence of self-pressure broadening in the $`\nu_{1} + \nu_{3}`$ band of acetylene"* (IN-PROCEEDINGS).  The two suites work together to fit a rotational line's line shapes in a vibrational transition, at a series of pressures, by minimizing a 3-dimensional fit function, based on the HTP.  

### pBroad.py
This program, using a configuration file (described [in the docs](docs), [example](examples/R9.conf)) take several raw data files, recorded by our DAQ.  Several options allowed for specific types of analysis.  Transmission data was converted to absorbance per pathlength (see paper for details), and a [ROOT](root.cern.ch) TTree was generated (described [in the docs](docs), [example](examples/R9.root)).

### gFit.py
Was our global fitting program.  The [ROOT](root.cern.ch) TTree Framework from pBroad was read.  A background spectum was generated, and a trial function was made. [iMinuit](https://github.com/scikit-hep/iminuit/tree/master)was used to minimize the trial function.  The function itself depends on a command line argument, which nulls specific parameters in the Hartmann-Tran Profile, which is mathematically described in [HTP.py](HTP.py).  More details are found in the original paper.

### Other Programs
All the other files in the respositiory are in support of these two programs.  

### Comments
Comments have been used ad nausium to explain code, and allow it to be followed.  All functions and classes contain [docstrings](https://www.python.org/dev/peps/pep-0257/) for simplicity.

## INSTALLATION
### Dependencies (External)
- This code was built on Python3.  Python2 has offically been closed on 1 Jan 2020.  I'm sorry it's not coming back.  There are quite a few features of 3 that this code takes advanatage of, and really limits it's functionallity on Python2.  You'd probably have to reprogram everything.

- ROOT installation info is found [on their website](root.cern.ch).  [The git](https://github.com/root-project/root) is my recommended way of installing  I've been running it on Release 6.16/00 successfully.  Make sure you have PyROOT options selected, and they are attached to Python3 (if you have both versions of Python installed on your computer).  Since this is a C++ library, it needs to be built.  

### Dependencies (Python Libraries)
The following packages are available with PyPI (pip command). 
- numpy
- scipy
- iminuit
- root_numpy

## Helpful links
The programs can be found / installed on PyPI
 - [iMinuit](https://github.com/scikit-hep/iminuit/tree/master).
 - [NumPy/SciPy](https://numpy.org/doc/1.18/index.html).
 - [root_numpy](https://github.com/scikit-hep/root_numpy).

ROOT's installation is a bit beyond the scope of this documentation, good luck.  However, it wouldn't be difficult to modify the code to remove ROOT's requirment.  I use it because it's what I grew up with.
 - [ROOT](root.cern.ch), [git](https://github.com/root-project/root).
 - [PyROOT](https://root.cern.ch/pyroot).
