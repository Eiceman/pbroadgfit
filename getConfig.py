#!/usr/bin/env python
"""This module is used by pBroad to read and parse the configurations files"""
#################################################
__author__=	"E. Gross"			#
__email__=	"eisen.gross@stonybrook.edu"	#
__date__=	"07 Feb 2020"			#
__version__=	"4.0.0 - STABLE"		#
#################################################
import numpy as np
from os import path
from array import array

# These are the options current allowed
boolOPs = ['pressLin', 'tempLin', 'length', 'pressCon']
#bool Options are True if in the file, else False
strgOPs = ['path', 'wmCorrect']
#strg Options are expected to have a string following
numbOps = ['']
#number Options (which haven't been used, expect a number to follow


## Dual File
#	delimit = \t
# Piezo voltage		(V)
# Transmission-I	(mV)	X
# Reference-I		(mV)	X
# Transmission-Q	(mV)	X
# Reference-Q		(mV)	X
# Pressure		(Torr)	gen	It never read this number, and it never really was there
# Temperature		(C)	gen	It never read this number, and it never really was there
# wavemeter		(GHz)	X
# wavemeter power	(mW)
# signal ratio		(arb)

## Comb File
#	delimit = ','
# PZT			(V)
# Rep. Rate		(Hz)
# Laser Frequency	(GHz)	X
# Comb Mode		(N)
# Ref I			(V)	X
# Ref Q			(V)	X
# Abs I			(V)	X
# Abs Q			(V)	X
# w/m freq		(GHz)
# w/m power		(W)
# Beat freq		(Hz)
# Beat Var		(Hz)

## Output FIle
# This is what pBroad makes
# nu			(cm-1)
# dvo			(cm-1)
# exp			(arb.)		# (Io/I) = exp(aL)
# raw			(arb.)		# ln(Io/I - baseline + 1) = aL
# abs			(arb.)		# aL / L = a 
# nor			(arb.)		# 1/N * a where N is int(a dv)
# Temps			(?K)
# Press			(?atm)
# file			(N)
def IQbal(I, Q, deg=90):
	"""IQbal(np.array I, np.array Q, deg=90)
	Using the In-phase and Quaditure arrays of the data, change the angle.
	The starting point doesn't need to be know, since it finds the 
	Magnitiude of the two points, and uses sin and cos to give it an angle.

	deg = 90 is default, since this is where I is maximized and Q is nulled
	"""
	rad = deg * (np.pi/180.)
	SigAmp = np.sqrt(Q**2 + I**2)

	nI = SigAmp * np.sin(rad)
	nQ = SigAmp * np.cos(rad)

	return nI, nQ
def smoothFreq(freq):
	"""smoothFreq(np.array x)
	Apply a second order poly fit to the freq, to smooth out the light frequency vs Piezo
	"""
	steps = np.asarray(list(range(len(freq))), dtype=int)
	co = np.polyfit(steps, freq, 2)
	fxn = np.poly1d(co)
	newFreq = fxn(steps)
	return newFreq

class File:
	"""class File(str name, int index, str file_path)
	Finds, file, and imports data to a numpy.array (strucutred).  The header is described at the top of this file
	Only certiain data is important to save, so only get that data.  
	"""
	def __init__(self, fname, index, path='.'):
		self.fNumber = index
		self.fName = fname
		self.loc = '/'.join([path, fname])
		self.yData = None				# Basically raw data
		self.Data = None				# Will be used after processing
		self.pathLength = 1.0				# Assume 1.0 cm path length

	def importData(self, ftype, comment=1):
		if ftype == 'dual':
			delimiter = '\t'
			header = [("PiezoVolt",		float),
					("TransI",		float),
					("RefI",		float),
					("TransQ",		float),
					("RefQ",		float),
					("Pressure",		float),
					("Temperature",		float),
					("LightFQ",		float),		#This is wavemeter, but it's the best we can do
					("wamemeterPwr",	float),
					("signalRatio",		float)]
		elif ftype == 'comb':
			delimiter = ','
			header = [("PiezoVolt",	float),
					("RepRateFQ",	float),
					("LightFQ",	float),
					("CombMode",	float),
					("RefI",	float),
					("RefQ",	float),
					("TransI",	float),
					("TransQ",	float),
					("wavemeter",	float),
					("wavemeterPwr", float),
					("CWBeatFQ",	float),
					("CWBeatAVar",	float)]
		else:
			raise ValueError("Unknown File Type {}.  STOPPING".format(ftype))

		data = np.loadtxt(self.loc, dtype=header, delimiter=delimiter, skiprows=comment)
		data = data[["LightFQ", "TransI", "RefI", "TransQ", "RefQ"]]
		self.yData = np.zeros(len(data), dtype=[("freq",float), ("TransI", float),("RefI", float),("TransQ", float),("RefQ", float), ("Press", float),("Temps",float)])
		self.yData["freq"] = data["LightFQ"]
		self.yData["TransI"] = data["TransI"]
		self.yData["TransQ"] = data["TransQ"]
		self.yData["RefI"]   = data["RefI"]
		self.yData["RefQ"]   = data["RefQ"]

	
	def expandPT(self, P, T):
		"""expandPT(list[n=2]* P, list[n=2]* T)
		Fills Pressure and Temp data array with linear spaced pressures and temperture data, can be the same value
		"""
		self.yData["Press"] = np.linspace(P[0], P[1], num=len(self.yData))
		self.yData["Temps"] = np.linspace(T[0], T[1], num=len(self.yData))
		print("    Press Temp linspaced")

	def massage(self):
		"""self.massage()
		Print status, sort data by frequency, give them to me in wavenumbers, and use IQbal to max(I) and null(Q)
		Smoothing the frequency isn't really needed, and could case problems if frequency spacing was large.
		"""
		print("  ### {} Captured ###  ".format(self.fName))
		self.yData.sort(order='freq', kind='heapsort')
		print("    Sorted  ")

		self.yData["freq"] = self.yData["freq"] * 1.e9 * (1./299792458.) * (1.e-2) 
		print("    Frequency GHz to Wavenumber")


		ti, tq = IQbal(self.yData["TransI"], self.yData["TransQ"], deg=90.)
		ri, rq = IQbal(self.yData["RefI"], self.yData["RefQ"], deg=90.)
		self.yData["TransI"]	= ti
		self.yData["RefI"] = ri
		self.yData["TransQ"] = tq
		self.yData["RefQ"] = ti 
		print("    IQ Balanced 90 degrees")

		#self.yData["freq"] = smoothFreq(self.yData["freq"])
		#print("    Frequency space smoothed")

class Config:
	def __init__(self,config_file):
		"""class Config(file)
		Open a configuration file, read it and parse the data.  See README for more details
		"""
		self.nu_0 = 0
		self.label = ""
		self.options = {'path':'.', 'pressLin':False, 'pressCon':False, 'tempLin':False, 'length':False, 'type':'dual', 'headerLength':1, 'wmCorrect':0}
		self._file = open(config_file, 'r')
		self.plist = None

		self.getHead()
		o, f = self.scanThrough()
		print("##### {} Captured #####".format(config_file))			# Closed config file
		print("\t {} @ {}".format(self.label, self.nu_0))			# Read and understood file
		self.parseOptions(o)
		print("\t TYPE: {}".format(self.options['type']))			# Read options
		f = self.checkFiles(f)
		print("\t ENTRIES: {}".format(len(f)))					# Read data files
		self.parseFiles(f)


	def getHead(self):
		"""getHead()
		reads line 1 and 2, that's literally it
		"""
		self.nu_0 = float(self._file.readline().strip())
		self.label = str(self._file.readline().strip())
		return
	
	def scanThrough(self):
		""" Scans the configure file, finds options and files, closes configure file at the end """
		options = []
		files = []
		for i in range(200):
			curr = str(self._file.readline().strip())
			if curr.startswith('#'):
				""" line is ignored """
				continue
			elif curr == '':
				""" A blank line is ignored """
				continue
			elif curr.startswith('.bk'):
				""" Option no long exists """
				print(".bk is obsolete, I didn't think I had any files that called it, ignoring")
				continue
			elif curr.startswith('.op'):
				""" This is a option setting """
				options.append(curr)
			elif curr.startswith('.END'):
				""" The end of the file """
				#NOTE: This might be the right place to have scan type note
				break
			else:
				""" No keyword means data """
				try:
					filestr, Pstr, Tstr, *etcstr = curr.split('\t')		# Bring in configuration line
					#if etcstr[0].isnumeric():				# This doesn't work in python for '-' or '.', regex to the rescue
					length = float(etcstr[0])
					#raise UserWarning("Config files has a 4+ line, but no length. Set length 1 \n {}".format(etcstr))

				except ValueError:
					filestr, Pstr, Tstr = curr.split('\t')			# Really old files with no length, or comments
					length = 1.0


				filestr.rstrip('.dat')				# If there is an extension we don't want to add it twice
				filestr = '.'.join([filestr, 'dat'])		# Add extension

				Pstr = Pstr.split(',')				# Pressure string "min(P),max(P)" or "all(P)"
				Tstr = Tstr.split(',')				# Temperature string "min(T),max(T)" or "all(T)"
				Pstr = [ float(Pstr[i]) for i in range(len(Pstr)) ]	# Make all values floats
				Tstr = [ float(Tstr[i]) for i in range(len(Tstr)) ]	# Make all values floats

				files.append([filestr, Pstr, Tstr, length])

		else:
			""" This runs at the end of a FOR loop but not if break is called"""
			print("I ran every line of my preset 200, and never found an .END statement, I'll assume I got them all") 

		self._file.close()
		return options, files

	def parseOptions(self, options):
		"""parseOptions(options)
		You got an option, I'll look into that for you.  Is it really a command? we'll see.
		option really is the line read by scanThrough thus I'll seperate the value from command if it's not boolean
		Also boolean is set TRUE.  NOT TOGGLED
		"""
		for i in range(len(options)):
			thisop = options[i].split()[1:]		#default split is all whitespace

			if thisop[0] in self.options.keys():
				if len(thisop) == 1:
					self.options[thisop[0]] = True
				else:
					self.options[thisop[0]] = thisop[1]
			else:
				raise UserWarning("Unknown options {}.  SKIPPING".format(thisop[0]))

	def checkFiles(self, files):
		"""checkFiles(files)
		If file can be found, add to array, else give error and keep going
		"""
		foo = self.options['path'].rstrip('/')			# Strips tailing / on path
		realFiles = []

		for i in range(len(files)):
			curFile = '/'.join([foo, files[i][0]])
			if path.exists(curFile) is True:
				realFiles.append(files[i])
			else:
				print("File {} not found in {}".format( files[i][0], foo))
		self.files = [None] * len(realFiles)
		return realFiles
				
	def parseFiles(self, files):
		"""parseFiles(files)
		Get array from checkFiles, seperates pressure, temp, and pathlength data, send filename to importData class and reads data
		"""

		self.plist = np.zeros(len(files), dtype=[('p0',float),('p1',float)])
		for i in range(len(files)):
			self.files[i] = File(files[i][0], i, path = self.options['path'])
			self.files[i].importData(self.options['type'], self.options['headerLength'])
			self.files[i].pathLength = files[i][3]
			
			P = files[i][1]
			T = files[i][2]	

			if len(P) == 1:
				P.append(P[0])					# If 1 value for P or T are given, make len(1) array into len(2) array with the same value
			if len(T) == 1:
				T.append(T[0])

			if len(P) != 2:						# If the above didn't fix 1 value arrays or there are more numbers then two.  Freak out a bit here. User should know better.
				raise ValueError("Pressure range is not len(2)?\n \t{}".format(files[i]))
			if len(T) != 2:
				raise ValueError("Temperture range is not len(2)?\n \t{}".format(files[i]))
			P = [float(P[i]) for i in range(2)]
			T = [float(T[i]) for i in range(2)]

			self.plist[i] = tuple(P)

			self.files[i].massage()
			self.files[i].expandPT(P,T)




