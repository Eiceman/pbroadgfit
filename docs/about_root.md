# ROOT TTree used 
The TFile should have a TTree named 'tt' with the follow TBranches:
## nu
Laser frequency in wavenumbers [$`cm^{-1}`$].
## dvo
Laser frequency minus transition energy in wavenumbers [$`cm^{-1}`$] ($`\nu - \nu_{0}`$).
## exp
$`\frac{I_{0}}{I_{T}}`$, I is the intensity of light from the detectors
## raw
ln(exp) = Absorbance 
## abs 
Absorbance per pathlength
## nor
Area Normalized 'abs' branch.  $`nor = \frac{1}{A} * abs`$, where $`A = \int abs \, d \nu`$,
## Temps
The temperature at the point in Celsius. 
## Press
The pressure at the point in units torr.
## file
A file index value.  Starting at 0.

