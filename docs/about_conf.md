### Configuration Files
xxx.conf
A configuration file is used by pBroad to pass arguments and list files.  They are written in plain text, and this section descibes how to use them.  

Required Lines, these lines are required to be read in order before any commands are read  
<table>
<tr><td>Line No.</td><td> Title 	</td><td> Description</td></tr> 
<tr><td>1 	</td><td> $`\nu_{0}`$ 	</td><td> line 1 is the transition center frequency.  This is the value subtracted to make the $`\nu-\nu_{0}`$ plots and other calculations must be a float. </td></tr>
<tr><td>2 	</td><td> label 	</td><td> line 2 is a short string used to name output files.  Typically this is the line transition like "R11"</td></tr> 
</table>  

Required Commands, after the lines above the rest of the file are commands, or files
<table>
<tr><td>Command </td><td> Description </td></tr>  
<tr><td>.END    </td><td> Must be found at the end of the file, otherwise it keeps reading the file.  (There is a failsafe, but still).</td></tr>
</table>

To use the following commands, anywhere in the file, start a line with a dot (.) and then the command word.  For boolean commands, that makes the command True.  Arguments follow the command with a space.  Options apply to the whole group of files, thus the last of two contradicting commands will be used.  

Options, strings or values  
<table>
<tr><td> Line No. 	</td><td> Title       	</td><td> Description </td></tr>
<tr><td>.path		</td><td>'./'	 	</td><td> The path to the datafiles, this is appended to the file name </td></tr>
<tr><td>.type		</td><td>'dual'	 	</td><td> Type of file used, see getConfig.py header for info, eiter 'dual' or 'comb'. <br/>
Note all our data is dual, but only some if comb, nice 4 letter variable names. </td></tr>
<tr><td>.wmCorrect	</td><td>0	 	</td><td> Shift frequency by wmCorrect.  This allows a constant correction to be added to the light frequency. </td></tr>
<tr><td>.headerLength	</td><td>1	 	</td><td> If there needs to be more than 1 line before the data, you can change that here. </td></tr>
</table>

Options, Booleans, default is always false
<table>
<tr><td>Command	</td><td> Description </td></tr>
<tr><td>.pressLin	</td><td> Pressure for this data is linearly increasing. Given min(p),max(p).<br/>
NOTE: The total pressure is increasing, not acetylene, so this was used early on, but was wrong. </td></tr>
<tr><td>.pressCon	</td><td> Pressure is corrected.  Determine the partial pressure for each scan, assume the first scan's initial pressure is the only fill of acetylene.  Makes a small difference <br/> 
NOTE: If pressLin and pressCon are both False, assume partial pressure of acetylene is initial pressure of each scan individually. </td></tr>
<tr><td>.tempLin	</td><td> Linearly interpolate temperture.  Never really used. </td></tr>
<tr><td>.length	</td><td> Don't assume pathlength was 1.  Always used. </td></tr>
</table>

Anyline that is not preceeded with a dot, is treated as a file.  A file has 3 or 4 columns, which are seperated by tabs.  Each position may have a comma seperated list.  Here's an example:  

    testfile1 &nbsp; 2.003,2.006 &nbsp; 24.3 &nbsp; 2.3

<table>
<tr><td> testfile1	</td><td> is the file name.  A '.dat' is appened to the end of the file name, and the value of the .path is appened first.</td></tr>
<tr><td>	      	</td><td> if .path = ./data then this would be read as: './data/testfile1.dat'</td></tr>
<tr><td> 2.003,2.006	</td><td> is the pressure of the file.  If this was 1 number with no comma p1 = p2.  See .pressLin and .pressCon for details</td></tr>
<tr><td> 24.3		</td><td> is the temperature of the file.  This too could be a list, with t1 and t2, but t1 = t2 since only 1 value is given.  See .tempLin for details</td></tr>
<tr><td> 2.3		</td><td> this is optional.  It's the pathlength of the cell.  Must be included if .length is set.   </td></tr>
</table>
Each of these fields is seperated by a tab.




