#!/usr/bin/env python
"""The module that contains the HTP function and the least squares fitting functions"""
#################################################
__author__=	"E. Gross"			#
__email__=	"eisen.gross@stonybrook.edu"	#
__date__=	"07 Feb 2020"			#
__version__=	"4.0.0 - STABLE"		#
#################################################
#from pygpmd.constants import constants as Con
from dualTool import constants as Con
from scipy.special import wofz as Faddeyeva
import numpy as np
class fitFunction:
	def __init__(self,numPts, vo, Sv, Mass, Elow, yvar=1e-5):
		"""class* fitFunction(numPts, vo, Sv, Mass, Elow, [yvar])
		fitFunction is a container class that allows all the functions for 
		generating and fitting data to a HTP

		INPUT
		numPts	int	Number of points (number of x's * number of pressures)

		vo	float	Center Line Center Frequency	(wavenumbers)
		Sv	float	Line Intensity @ 296K - Used for amp = Sv*N*Length (HITRAN Units)
		Mass	float	Mass of absorber in kg
		Elow	float	Lower Energy level (found in HITRAN) (wavenumbers), used for Sv(T)

		yvar	float	OPTIONAL yvariance in fit, 1e-4 is typical - removed

		Returns Class

		To generate Line Shape
			fxn = fitFunction(...)
			fxn.x = Data
			line = fxn.HTP(fxn.x[p,:], PARAMETERS)		for x at pressure
				OR
			fxn.gHTP(PARAMETERS)				for all x
			line = fxn.v 

		To fit, fill both x and y.  The class was built with MINUIT in mind.
			see iminuit for details
		"""
		dt = [
				('x', np.float64),('y', np.float64),('v', np.float64),
				('p', np.float64),('t', np.float64),('i', np.int8),
				]
		self.Data = np.zeros(numPts, dtype = dt)
		#self.Data['y'] = 1			# Set eveything here to 1		Transmission
		self.Data['y'] = 0			# Set eveything here to 0		Absorption

		self.vo = vo				# Line Center 
		self.Sv = Sv				# Spectral Line Intensity @ 296K (defined in HITRAN)
		self.Elow = Elow
		self.Mass = Mass			# Mass of the molecule in kg
		self.yvar = np.ones(numPts, dtype=float) * yvar

		self.diff = np.zeros(len(self.Data), dtype=float)	# NOTE: For debug

	def onePressure(self, i):
		"""onePressure(i)
		Given an index from Data['i'] 

		Returns Array's x, y, v
		and floats
		P, T
		"""
		theseRows = self.Data['i'] == i
		x = self.Data['x'][theseRows]
		y = self.Data['y'][theseRows]
		v = self.Data['v'][theseRows]
		p = np.unique(self.Data['p'][theseRows])
		t = np.unique(self.Data['t'][theseRows])
		"""
		if type(p) != float:
			p = p[0]
			t = t[0]
			raise RuntimeWarning("onePressure matched many rows, using index 0")
		"""
		return x, y, v, p, t

	def HTP(self, x, Gam0, Shift0, Gam2, Shift2, eta, anuVC, Amp, T):
		"""HTP(x, Gamma0, Shift0, Gamma2, Shift2, eta, anuVC, Amp, Temp)
		Hartmann-Trans Profile, based on the following works:
			- Tennyson et al., Pure Appl. Chem., 86 (12), 1931-1943 (2014)
			- H. Tran, N. H. Ngo, and J.-M. Hartmann, J. Quant. Spectrosc. Radiat. Transf. 129, 199 (2013).
			- N. H. Ngo, D. Lisak, H. Tran, and J.-M. Hartmann, J. Quant. Spectrosc. Radiat. Transf. 129, 89 (2013).
		Output
		This function is used to generate a TRANSMISSION, area normalized line shape based on the
		HTP model.  Other lineshapes have the following realationships:
			- Voigt - fix gamma2, shift2, eta, anuVC to 0
			- SDVoigt - fix eta, anuVC to 0
		
		Input Parameters (all floats except x)
			x	[cm^-1]	Frequency in wavenumbers		[numpy array]
			Gamma0	[cm^-1]	Speed Averaged Line Width
			Shift0	[cm^-1]	Speed Averaged Line Shift (Delta)
			Gamma2	[cm^-1]	Speed Dependence of Line Width
			Shift2	[cm^-1]	Speed Dependence of Line Shift 
			eta	[cm^-1]	Correlation Parameter
			anuVC	[cm^-1]	Velocity Changing Frequency (nu_vc)
			Amp	[NONE]	Amplitude of signal (see note below)
			Temp	[K]	Temperature in K

		For exact values, Amplitude is:
			Length * Pressure * Sv(T) * N(T,P)
			Sv(T)	Spectral Line Intensity at temperature T			[cm^-2 / (molecule * cm^-2]
			N(T,P)	Number Density of the gas at temperature T and pressure P	[molecules/cm^3]	
		Note capital Gamma and Delta are gamma*P and delta*P respectively, meaning units are all wavenumbers
		"""
		va0 = np.sqrt(2*Con.k*T/self.Mass) # Most Probable speed with mass M [kg]
		cte = Con.c/(self.vo*va0)	# Constant c2 == sqrt(ln(2))/Gamma_D

		C0 = complex(Gam0, Shift0)
		C2 = complex(Gam2, Shift2)

		C0t = (1-eta) * (C0-3*C2/2) + anuVC
		C2t = (1-eta) * C2

		if C2t == 0:
			Zm = (1j * (self.vo -x) + C0t)*cte
			FadZm = Faddeyeva(1j*Zm)
			BHTP = np.sqrt(np.pi)*cte*((1-Zm**2)*FadZm + Zm/np.sqrt(np.pi))
			AHTP = np.sqrt(np.pi)*cte*FadZm
			LineTop = AHTP
			LineBot = 1 - (anuVC - eta*(C0-3*C2/2))*AHTP + eta*C2*BHTP
		else:
			X = (1j * (self.vo-x) + C0t) / C2t
			Y = (self.vo * va0/(2*Con.c*C2t))**2
			Zp = np.sqrt(X+Y) + np.sqrt(Y)
			Zm = np.sqrt(X+Y) - np.sqrt(Y)
			FadZm = Faddeyeva(1j*Zm)
			FadZp = Faddeyeva(1j*Zp)
			
			BHTP = va0**2/C2t*(-1 + np.sqrt(np.pi)*(1-Zm**2)*FadZm/(2*np.sqrt(Y)) -\
				np.sqrt(np.pi)*(1-Zp**2)*FadZp/(2*np.sqrt(Y)))

			AHTP = np.sqrt(np.pi)*Con.c*(FadZm - FadZp)/(self.vo*va0)

			LineTop = AHTP;
			LineBot = 1 - (anuVC-eta*(C0-3*C2/2))*AHTP + (eta*C2/(va0**2))*BHTP;

		#LineGen = np.exp(Amp/np.pi * np.real(LineTop/LineBot))				# Transmission
		LineGen = Amp/np.pi * np.real(LineTop/LineBot)					# Absorption

		return LineGen

	def ParticleDensity(self, Press, Temp):
		"""ParticleDensity(*float Pressure, *float Temperature [K])
		Input 
			Pressure [atm], Temperature [K]
		Calculates the Particle Density using the ideal gas law [molecules/cm^3]
		"""
		T = Temp				# Must be in Kelvin, might as well do it here
		a = (100./1)**3 * (1./1000) * (1./101.325)	# J = m^3*Pa -> cm^3 * atm
		R = Con.R * a	
		k = Con.k * a
		
		#nV = Press / (R*T)			# moles
		nV = Press/ (k*T)			# molecules
		return nV

	def Tips(self, Temp):
		"""Tips(*float Temperature [K])
		Total Internal Partition Sums.  Simplified by a polynomial
		per R.R. Gamache, et. al. 10.1016/S0022-2860(99)00266-5
		"""
		#    T^3,	T^2,	    T,	    constant
		c = [8.4612e-6, -2.5946e-3, 1.4484, -8.3088]
		Q = np.polyval(c, Temp)
		return Q

	def SvT(self, Sv, Temp, Elow):
		"""SvT(*float Sv @ 296K, *float Temperature [K], *float E" transition, *float nu_ij)
		HITRAN uses Sv(T=296K) as a standard constant.  But it's
		Temperature dependent.  This changes it per the equation 
		given in the description. 
		https://hitran.org/docs/definitions-and-units/ for details
		"""
		c = (Con.h * Con.c)/Con.k * 100		#c2 constant in cm*K
		vij = self.vo
		Eup = Elow + self.vo

		Q = self.Tips(296)/self.Tips(Temp)
		A = ( np.exp(-c*Eup / Temp) ) / ( np.exp(-c*Eup/(296)) )
		B = ( 1 - np.exp(-c*vij/ Temp) ) / ( 1 - np.exp(-c*vij/(296)) )
		
		S = Sv * (Q * A * B)

		return S

	def baseline(self, x, a, b, c):
		"""baseline(x, a, b, c)
		Very Simple polynomial baseline of the 1st order solved at x
		a = offset
		b = slope
		that way if we add more, we can just add it to the end
		"""
		y = a + b*x + c**2
		return y
	def surfBase(self, x, p, a, b, c, d, e):
		"""surfBase(x, p, b0, b11, b12, b21, b22)
		Calculates a simple 2D surface with parameters:
			b0	offset
			b11	x slope
			b12	x parabolic
			b21	p slope
			b22	p parabolic
		given arrays x and p
		"""
		freq = b*x + d*x**2
		pres = c*p + e*p**2
		base = a + freq + pres
		
		return base

	def gHTP(self, Gam0, Shift0, Gam2, Shift2, eta, anuVC, L, w0=0):
		"""gHTP(gamma0, shift0, gamma2, shift2, eta, anuVC, Length, OPTIONAL w0)
		*g*lobal *HTP* is the function used to minimize the global function

		Given the *LOWER CASE* parameters gamma0, gamma2, shift0, shift2, that is
		the pressure dependent in units [cm*-1/atm].  The global multi-spectrum is 
		calculated.

		self.Data must be filled:
			self.Data['x']	[cm^-1]		X-axis points
			self.Data['t']	[K]		Temperature at point
			self.Data['p']	[atm]		Pressure at point

		This is also where Amplitude is calculated.  Amp = -1* SNL for transmission

		Optional w0 allows for some error in the wavemeter by x + w0

		Because of how HTP is expecting 1 line at a time, this program breaks it up into 
		chunks. Split by Pressure, see self.onePressure().

		NOTE: Sv and Particle Density are calculated with the Pressure and Temperature,
		those only L can be used to add variation in the amplitude.  But l/L ~ sv/Sv thus 
		the change in a known parameter like L could be used to later determine a change in
		Sv
		"""
		Line = np.zeros(len(self.Data), dtype = float)
		pt = 0
		pmax = np.unique(self.Data['p'])
		for p in range(len(pmax)):
			x, y, v, P, T = self.onePressure( p)
			ii = pt
			jj = pt + len(x)

			gam0 = Gam0 * P
			shift0 = Shift0 * P
			gam2 = Gam2 * P
			shift2 = Shift2 * P
			Eta = eta * P
			AnuVC = anuVC * P
			
			N = self.ParticleDensity(P, T)
			S = self.SvT(self.Sv, T, self.Elow)
			#SNL = -1 * L * S * N				# Transmission needs a -1, not ABS
			SNL = L * S * N

			Line[ii:jj] = self.HTP((x+w0), gam0, shift0, gam2, shift2, Eta, AnuVC, SNL, T)

			pt = jj

		return Line

	def gbHTP(self, Gam0, Shift0, Gam2, Shift2, eta, anuVC, L, w0, b0, b1, b2, b3, b4):
		"""gbHTP(self, Gam0, Shift0, Gam2, Shift2, eta, anuVC, L, w0, b0, b1, b2, b3, b4)
		See gHTP.  Uses that to calculate the line.  Addes baseline
		Baseline is a 2D parabolic surface

		"""

		Line = np.zeros(len(self.Data), dtype = float)
		iix = np.linspace(0, len(self.Data)-1, num=len(self.Data))
		Base = np.zeros(len(self.Data), dtype = float)

		Line = self.gHTP(Gam0, Shift0, Gam2, Shift2, eta, anuVC, L, w0)
		#Line = self.hHTP(Gam0, Shift0, Gam2, Shift2, eta, anuVC, L, w0)	# If you want to use HAPI

		Base = self.surfBase(iix, self.Data['p'], b0, b1, b2, b3, b4)

		return Line + Base

	def lsqHTP(self, Gam0, Shift0, Gam2, Shift2, eta, anuVC, L, w0, b0, b1, b2, b3, b4):
		"""lsqrHTP(gamma0, shift0, gamma2, shift2, eta, anuVC, Length, w0, b0, b1, b2)
		This function is used to calculate the least squares of the single row fit
		self.yvar can change it weight.  This was built to use iMinuit 

		see HTP for details on parameters.  b0 and b1 are baseline parameters, in increaseing order
		L is length.  SvN is calculated, Amp = SvNL thus, L can be used for some adjustments
		"""
		yvar = self.yvar
		#self.v = self.gHTP(Gam0, Shift0, Gam2, Shift2, eta, anuVC, L )[p,:]
		self.Data['v'] = self.gbHTP(Gam0, Shift0, Gam2, Shift2, eta, anuVC, L, w0, b0, b1, b2, b3, b4)

		self.diff = (self.Data['y'] - self.Data['v'])**2
		lsq = (self.diff/yvar)
		Lsq = sum(lsq)
		return Lsq

