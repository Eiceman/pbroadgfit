# How to Contribute Code

Thank you for you interest. I know this code could really benefit from optimization, and organization.  If you are interested in contributing to this code, there are generally two rules I'm asking for.

1. Comment as best you can, whereever you can.  I made it a bit push to leave comments in the code.  I think DocStrings is a great tool in Python, and would strongly recommend any functions or classes to use them.  
2. Keep it on the dev branch.  I'm not sure what time I may dedicate to this code base.
3. Tags will represent version and subversions.  Keeping track what version a split is based off is usefull.  You'll notice that version numbers are in the headers of all files.
