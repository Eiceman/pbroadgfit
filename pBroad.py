#!/usr/bin/env python
"""This is the main program that actually reads the raw data files and finds the absorbance.  
It writes the output to ROOT TTree"""
#################################################
__author__=	"E. Gross"			#
__email__=	"eisen.gross@stonybrook.edu"	#
__date__=	"07 Feb 2020"			#
__version__=	"4.0.0 - STABLE"		#
#################################################
from root_numpy import array2root
from dualTool import norm 
from getConfig import Config
import pressureCorrection
import argparse
import numpy as np

argpars = argparse.ArgumentParser(
		prog = 'pBroad.py',
		description = "Reads configuration file (see docs) and imports data using pyGPMD library. \
				Determines pressure and tempertures, and expands them with a linspace\
				Uses a sin*sin function to smooth Ref data and remove etalon from Tran channel\
				Calculates absorption, and further smooths.  Exports to ROOT TTree but can\
				be modified for csv export.  Uses MINUIT function for fitting!")
argpars.add_argument('configFile', type=str, help="A very complex file")
args = argpars.parse_args()

config = Config(args.configFile)
datafiles = config.files
### Config is pretty blind to options types.  So this is where they can be changed
config.options['wmCorrect'] = float(config.options['wmCorrect'])			# str->float

### A numpy dtype list of the expected output sturcture
outtypes = [
		('nu',float),		# nu : cm^{-1}
		('dvo',float),		# nu - nu_0 : cm^{-1}
		('exp',float),		# (Io/I) = exp(aL)
		('raw',float),		# ln(Io/I) = aL
		('abs',float),		# aL / L = a 
		('nor',float),		# 1/N * a where N is int(a dv)
		('Temps',float),		# linear extroplation of temperature
		('Press',float),		# linear extroplation of pressure
		('file',int)]		# a int representing the file

pts = 0
if config.options['pressLin'] is not True:
	Pressures = pressureCorrection.main(config.plist)

for i in range(len(datafiles)):
	thesepts = len(datafiles[i].yData)
	thisData = np.zeros(thesepts, dtype=outtypes)
	pts += thesepts

	# Dealing with Pressure linear Option
	#   NOTE:in the July 2019 version of pBroad a much more complicated pressure calc was used 
	#   We weren't bringing the volume to vacuum between runs which had to be accounted for.  
	#   I will add this feature back in here for historic reasons.  And the option will be added for it

	if config.options['pressLin'] is True:
		thisData["Press"] = datafiles[i].yData["Press"]				# This array is already linearilzed
											# Since p. pressure of acetylene is constant, this is a terrible thing to do.
	else:
		if config.options['pressCon'] is True:					# If the gas wasn't vacuumed between scans, p. press of acetylene != P[0]
			thisData["Press"] = np.ones(thesepts) * Pressures["ppress"][i]	# So we can find the real p. pressure here
		else:
			thisData["Press"] = np.ones(thesepts) * Pressures["pa"][i]	# Or, maybe it is the first pressure recorded.  When you do vacuum between scans

	# Dealing with Temperature linear Option
	if config.options['tempLin'] is True:
		thisData["Temps"] = datafiles[i].yData["Temps"]				# This array is already linearilzed
	else:
		thisData["Temps"] = np.ones(thesepts) * datafiles[i].yData["Temps"][0]	# Otherwise we can just take the first entry

	# Dealing with Path Length Option, which should never have been an option
	if config.options['length'] is True:	
		pathLength = datafiles[i].pathLength					# Get pathlength per file! in cm
	else:
		pathLength = 1.0							# Pathlength is 1 cm
	#print("\t got pathlegth {}".format(pathLength))

	thisData["file"] = np.ones(thesepts, dtype=int) * datafiles[i].fNumber		# Expands an file index number to whole array

	if config.options['wmCorrect'] != 0:
	# Import Laser Frequency.  For dual = wavemeter, for comb = comb calculated,
		thisData["nu"] = datafiles[i].yData["freq"] + config.options['wmCorrect']	
	else:
		thisData["nu"] = datafiles[i].yData["freq"] 
											# Wavemeter Correction is added here
	thisData["dvo"] = datafiles[i].yData["freq"] - config.nu_0			# nu - nu_0 = d(nu_0) So 0 = transistion energy

	# Now that was the easy copy and paste kind of stuff.  Now for the real equations
	# During the file import routine, we balanced our IQ data such that Q = 0 in phase
	transmission = datafiles[i].yData["TransI"] / datafiles[i].yData["RefI"]	# I/Io = Transmission, making data fractional during more absorption
	trans_pedSub = (transmission - max(transmission) ) +1				# transmission with pedistal subtraction.  T - max(T) is  ‾\/‾ starting at 0. +1 shifts it up

	absorbance = np.log( trans_pedSub **-1)						# I think this is better called optical density, but absorbance is more accecpted
											# -Log(Trans) = Log(e^({-Abs*Sv*N}*Length)) = {-Abs*Sv*N} *Length
	#absorbance = np.log( transmission**-1)						# Sometimes you don't need subtraction
	absorption = absorbance / pathLength						# A = Ecl (Beer's Law).  Thus we have A/l = Ec and E is the molar attenuation which is what we're after
											# Because that's the function of nu and the line shape
											# doi:10.1006/jmsp.2001.8426, a = ln(Io/I)/L
											#	a = S*N*g(v), g := norm(voigt)

											
	AbsTrapz, U, N = norm(absorption, thisData["dvo"])			# Normalize by area

	# Tie this all up with a bow 
	thisData["raw"] = transmission							# I/Io = exp(aL)
	thisData["exp"]	= absorbance							# aL = ln(Io/I - baseline +1)
	thisData["abs"] = absorption							# a = aL/L
	thisData["nor"] = AbsTrapz							# 1/N * a; N = integrate(a dv)

	# Send it off and hope they recieve before the holidays
	datafiles[i].Data = thisData


Out = np.zeros(pts, dtype=outtypes)

l = 0
for i in range(len(datafiles)):
	k = len(datafiles[i].Data) + l
	#print([l, k])

	Out['nu'][l:k]		= datafiles[i].Data['nu'][:]
	Out['dvo'][l:k]		= datafiles[i].Data['dvo'][:]
	Out['Temps'][l:k]	= datafiles[i].Data['Temps'][:]
	Out['Press'][l:k]	= datafiles[i].Data['Press'][:]
	Out['file'][l:k]	= datafiles[i].Data['file'][:]

	Out['exp'][l:k]		= datafiles[i].Data['exp'][:]
	Out['abs'][l:k]		= datafiles[i].Data['abs'][:]
	Out['raw'][l:k]		= datafiles[i].Data['raw'][:]
	Out['nor'][l:k]		= datafiles[i].Data['nor'][:]

	l = k

#cutdvo = 0.03
#Out = Out[ abs(Out['dvo']) < cutdvo]
#print("CUTTING, dvo E [{}, {}]".format(-1*cutdvo, cutdvo))

array2root(Out, "{}.root".format(config.label), 'tt', mode='recreate')
