#!/usr/bin/env python
"""The module contains serveral functions that are used repeatedly throughout the programs"""
#################################################
__author__=	"E. Gross"			#
__email__=	"eisen.gross@stonybrook.edu"	#
__date__=	"07 Feb 2020"			#
__version__=	"4.0.0 - STABLE"		#
#################################################
import numpy as np
from ROOT import TGraph, TGraph2D, TGraphErrors
from array import array

class constants:
	"""Constants 
	Just a class that contains 2019 SI unit constants, and a few common convertion factors
	"""
	# Fixed Constants - 2019 SI Units
	dv_Cs = 9192631770		# Hz - Cs GND hyperfine transition freq 
	c = 299792458.			# m/s - Speed of light
	h = 6.62607015e-34		# J*s - Plank's Constant
	k = 1.3806493e-23		# J/K - Boltzmann Constant 
	N = 6.02214076e23		# /mole - Avogadro Constant	
	e = 1.602176634e-19		# C - Elementary Charge
	R = 8.3144598			# Gas Constant J/(K*mol)

	# Converters
	uma = 1.660538782e-27   	# kg/amu
	MtC = 1.0e6/(c*100)		# cm^-1 / MHz converter
	tPa = 133.322368421		# Pa / Torr
	aPa = 101325			# Pa / atm

def nDensity(P, T):
	"""nDensity(float* Pressure [Torr], float* Temperature [K]
	Takes the Pressure and Temperature and calculates it's number density
	using the ideal gas law.  
	Returns in units molecules/cubic centimeter
	"""

	nDensity = P/ (constants.k * T)		#PV = N kb T -> N/V = num Density in stupid units
	nDensity *= 1./constants.tkPa * ((1./10.)**3)	#Now in molecules / cm^3
	return nDensity

def rootGraph2D(xx, yy, zz):
	"""rootGraph2D(np.array x, np.array y, np.array z)
	Takes in 3 np.array's with same length!
	Converts np.array -> array.array, and uses generates 
	a ROOT TGraph2D Object, with points (x, y, z).
	"""
	x = array('d')
	y = array('d')
	z = array('d')
	x.fromlist(xx.tolist())
	y.fromlist(yy.tolist())
	z.fromlist(zz.tolist())
	gg = TGraph2D(len(x), x, y, z)
	return gg

def rootGraph(xx, yy):
	"""rootGraph(np.array x, np.array y)
	Takes in 2 np.array's with same length!
	Converts np.array -> array.array, and uses generates 
	a ROOT TGraph Object, with points (x, y).
	"""
	x = array('d')
	y = array('d')
	x.fromlist(xx.tolist())
	y.fromlist(yy.tolist())
	gg = TGraph(len(x), x, y)
	return gg
def rootGraphE(xx, yy, ex=None, ey=None):
	"""rootGraph(np.array x, np.array y, [np.array dx], [np.array dy])
	Takes in 2 to 4 np.array's with same length!
	Converts np.array -> array.array, and uses generates 
	a ROOT TGraphErrors Object, with points (x, y).
	dx and dy are errors of a point, and if none is given, the vector is 0,
	the values are equal to 1 side of error, meaning x=0, ex=0.5, 
		the errorbars will be 1 in length, center on a point 0.
	"""
	x = array('d')
	y = array('d')
	e = array('d')
	f = array('d')
	x.fromlist(xx.tolist())
	y.fromlist(yy.tolist())
	if ex is None:
		e.fromlist(np.zeros(len(xx)).tolist())
	else:
		e.fromlist(ex.tolist())
	if ey is None:
		f.fromlist(np.zeros(len(yy)).tolist())
	else:
		f.fromlist(ey.tolist())

	gg = TGraphErrors(len(x), x, y, e, f)
	return gg

def norm(y, x):
	"""norm(np.array* y, np.array* x)
	Yes it's backwards, for reasons!!!

	returns y1 =1/(np.trapz(y,x)) * y
	returns y2 = 1/max(y1) * y1
	returns np.trapz(y,x)

	Take the x and y data.  Using trapezoidal composite inegrate (from NP) calculate the area
	Divide the data by this normalization function.
	Then take the max of this value, and divide by that: making max(y) == 1, returns this 
	with the original area.

	This can be easily modified (duh!)
	"""
	#Normalize by area
	N = np.trapz(y, x=x)
	y1 = (1./N) * y

	#Scale to 1
	M = max(y1)
	y2 = (1./M) * y1

	#y2 = (1./M) * y		# Just scale to 1 with no area norm (ewww)

	#return y2, N			# Give me some Normalization with a side of scaleing
	#return y1, N			# Give me some Normalization hold the scaleing
	return y1, y2, N		# Area, Scaled + Area, Norm Constant
